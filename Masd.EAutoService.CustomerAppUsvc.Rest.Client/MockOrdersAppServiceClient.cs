﻿using Masd.EAutoService.CustomerAppUsvc.Rest.Client.CustomerClient;
using Masd.EAutoService.CustomerAppUsvc.Rest.Client.MechanicClient;

using Masd.EAutoService.CustomerAppUSvc.Rest.Model;


namespace Masd.EAutoService.CustomerAppUsvc.Rest.Client
{
    public class MockOrdersAppServiceClient : IOrdersAppService
    {
        

        public static OrderAppDTO[] ordersDTO = new OrderAppDTO[] {
            new OrderAppDTO { Id=1, MechanicName="Robert", MechanicSurname="Winnicki", CustomerName="Stefan", CustomerSurname="Wyszynski",
                IsFinished=true, ServiceList = new List<String>{"Oil change", "Tireschange" } },
            new OrderAppDTO { Id=2, MechanicName="Orlando", MechanicSurname="Merloni", CustomerName="Adam", CustomerSurname="Rybalko",
                IsFinished=true, ServiceList = new List<string>{"Tireschange"} },
            new OrderAppDTO { Id = 3, MechanicName = "", MechanicSurname= "", CustomerName="", CustomerSurname="",
                    ServiceList = new List<string> {"a", "d", "s"}, IsFinished = false}
        };

        

        public static ServiceDTO[] servicesDTO = new ServiceDTO[] { new ServiceDTO { ServiceId = 1, Name = "Oil change", Price = 20.00 }, new ServiceDTO { ServiceId = 2, Name = "Filter Change", Price = 15.25 }, new ServiceDTO { ServiceId = 3, Name = "Breaks Change", Price = 19.99 }, new ServiceDTO { ServiceId = 4, Name = "Tires change", Price = 9.99 } };

     

        public ServiceDTO[] GetServices()
        {
            
            
            return servicesDTO;
        }

        public ServiceDTO GetService(int id)
        {
            return servicesDTO.FirstOrDefault(m => m.ServiceId == id)!;
        }

        public void MakeOrder(List<int> serviceList, string name, string surname)
        {
            NewCustomer(name, surname);

        }

        public int GetCustomerId()
        {
            return MockCustomersDataServiceClient.customersDTO.Last().Id + 1;
        }
        public int GetMechanicId()
        {
            Random rnd = new Random();
            return MockMechanicsDataServiceClient.mechanicsDTO[rnd.Next(0, MockMechanicsDataServiceClient.mechanicsDTO.Length)].Id;
        }
        public int GetOrderId()
        {
            return MockOrdersDataServiceClient.ordersDTO.Last().Id + 1;
        }

        public void NewCustomer(string name, string surname)
        {
            int id = MockCustomersDataServiceClient.customersDTO.Last().Id + 1;

            MockCustomersDataServiceClient.customersDTO.Append(new CustomerDTO { Id = id, Name = name, Surname = surname });
        }
        public OrderAppDTO GetOrder(int id)
        {
            return ordersDTO.FirstOrDefault(m => m.Id == id)!;
        }

    }
}
