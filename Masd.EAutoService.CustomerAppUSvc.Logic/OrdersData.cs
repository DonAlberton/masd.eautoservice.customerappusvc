﻿using Masd.EAutoService.CustomerAppUsvc.Rest.Client.ServiceClient;
using Masd.EAutoService.CustomerAppUSvc.Model;
using Masd.EAutoService.CustomerAppUsvc.Rest.Client.CustomerClient;
using Masd.EAutoService.CustomerAppUsvc.Rest.Client;
using Masd.EAutoService.CustomerAppUSvc.Rest.Model;

namespace Masd.EAutoService.CustomerAppUSvc.Logic
{
    public class OrdersData : IOrdersData
    {
        private static readonly object orderLock = new object();
        private static readonly object serviceLock = new object();

        ServicesDataServiceClient serviceClient = new ServicesDataServiceClient();
        OrdersDataServiceClient orderClient = new OrdersDataServiceClient();
        MechanicsDataServiceClient mechanicClient = new MechanicsDataServiceClient();
        CustomersDataServiceClient customerClient = new CustomersDataServiceClient();

        /*private static IServicesDataService serviceClient = new MockServicesDataServiceClient();
        private static ICustomersDataService customerClient = new MockCustomersDataServiceClient();
        private static IOrdersDataService orderClient = new MockOrdersDataServiceClient();
        private static IMechanicsDataService mechanicsClient = new MockMechanicsDataServiceClient();*/

        static OrdersData()
        {
            lock (orderLock)
            {

            }
        }

        public void MakeOrder(List<int>? serviceList, string name, string surname)
        {
            CustomerDTO[] customers = GetCustomers();
            CustomerDTO customer = customers.FirstOrDefault(m => m.Name == name && m.Surname == surname);
            int customerId;
            if (customer != null) 
            {
                customerId = customer.Id;
            }
            else
            {
                NewCustomer(name, surname);
                customerId = GetCustomerId();
            }
           
            int mechanicId = GetMechanicId();

            orderClient.AddOrder(mechanicId, customerId, serviceList);

        }

        public ServiceDTO[] GetServices()
        {
            return serviceClient.GetServices();
        }
        public ServiceDTO GetService(int id)
        {
            return serviceClient.GetService(id);
        }

        public int GetCustomerId()
        {
            return customerClient.GetCustomers().Last().Id;
        }
        public CustomerDTO[] GetCustomers()
        {
            return customerClient.GetCustomers();
        }

        public int GetMechanicId()
        {
            Random random = new Random();
            MechanicDTO[] mechanicDTOs = mechanicClient.GetMechanics();
            return mechanicDTOs[random.Next(0, mechanicDTOs.Length)].Id;
        }
        public void NewCustomer(string name, string surname)
        {
            customerClient.AddCustomer(name, surname);
        }



        public Order GetOrder(int id)
        {
            lock (orderLock)
            {
                OrderConverter converter = new OrderConverter(mechanicClient,
                                              customerClient,
                                              orderClient,
                                              serviceClient);

                Order[] convertedOrders = converter.Convert().ToArray();
                return convertedOrders.FirstOrDefault(m => m.Id == id);
            }
        }

    }
}