﻿using Masd.EAutoService.CustomerAppUSvc.Rest.Model;


namespace Masd.EAutoService.CustomerAppUSvc.Model
{
    public interface IOrdersData
    {
        public void MakeOrder(List<int>? serviceList, string name, string surname);

        public ServiceDTO[] GetServices();

        public ServiceDTO GetService(int id);
        public Order GetOrder(int id);
    }
}
