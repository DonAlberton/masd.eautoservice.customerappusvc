﻿using Masd.EAutoService.CustomerAppUsvc.Rest.Client;
using Masd.EAutoService.CustomerAppUSvc.Rest.Model;

namespace Masd.EAutoService.CustomerAppUSvc
{
    public class Program
    {

        public static void Main(string[] args)
        {
            OrdersAppServiceClient client = new OrdersAppServiceClient();
            MockOrdersAppServiceClient mockClient = new MockOrdersAppServiceClient();
            
             Console.WriteLine("client response:");
             Console.WriteLine(client.GetService(1).Name);
             Console.WriteLine("mockClient response:");

             Console.WriteLine(mockClient.GetService(1).Name);
             Console.WriteLine();

             Console.WriteLine("mockClient response:");
             foreach (ServiceDTO service in mockClient.GetServices())
             {
                 Console.WriteLine(service.Name);
             }
             Console.WriteLine("client response:");

             foreach (ServiceDTO service in client.GetServices())
             {
                 Console.WriteLine(service.Name);
             }
             Console.WriteLine(mockClient.GetServices()[2].Name);
           
        }
    }
}