﻿namespace Masd.EAutoService.CustomerAppUSvc.Rest.Model
{
    public class ServiceDTO
    {
        public object serviceDTO;

        public int ServiceId { get; set; }
        public string? Name { get; set; }
        public double Price { get; set; }
    }
}
