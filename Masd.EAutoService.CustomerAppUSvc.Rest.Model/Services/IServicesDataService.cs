﻿namespace Masd.EAutoService.CustomerAppUSvc.Rest.Model
{
    public interface IServicesDataService
    {
        ServiceDTO GetService(int id);
        ServiceDTO[] GetServices();
    }
}
