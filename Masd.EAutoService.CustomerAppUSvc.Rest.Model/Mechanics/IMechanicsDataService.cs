﻿namespace Masd.EAutoService.CustomerAppUSvc.Rest.Model
{
    public interface IMechanicsDataService
    {
        public MechanicDTO[] GetMechanics();
        public MechanicDTO GetMechanic(int id);
    }
}