using Masd.EAutoService.CustomerAppUSvc.Logic;
using Masd.EAutoService.CustomerAppUSvc.Model;
using Masd.EAutoService.CustomerAppUSvc.Rest.Model;

using Microsoft.AspNetCore.Mvc;

namespace Masd.EAutoService.CustomerAppUSvc.Rest.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class OrdersAppController : ControllerBase, IOrdersData
    {
        private readonly ILogger<OrdersAppController> logger;
        private readonly IOrdersData ordersData;

        public OrdersAppController(ILogger<OrdersAppController> logger)
        {
            this.logger = logger;
            ordersData = new OrdersData();
        }



        [HttpPost]
        [Route("MakeOrder")]
        public void MakeOrder(List<int>? serviceList, string name, string surname)
        {
            ordersData.MakeOrder(serviceList, name, surname);
        }

        [HttpGet]
        [Route("GetServices")]
        public ServiceDTO[] GetServices()
        {
            return ordersData.GetServices();
        }
        [HttpGet]
        [Route("GetService")]
        public ServiceDTO GetService(int id)
        {
            return ordersData.GetService(id);
        }

        [HttpGet]
        [Route("GetOrder")]
        public Order GetOrder(int orderId)
        {
            return ordersData.GetOrder(orderId);
        }

    }
}